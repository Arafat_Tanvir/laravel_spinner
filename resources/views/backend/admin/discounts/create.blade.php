@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Dicount 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('discount_index') }}" class="btn btn-success"> All Dicount List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Barnd Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('discount_store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
                <label for="product_id" class="col-sm-3">Product</label>
                <div class="form-input col-sm-9">
                    <select name="product_id" id="product_id" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Product===</option>
                            @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->product_title}}</option>
                            @endforeach
                    </select>
                    <div class="valid-feedback">
                      {{ ($errors->has('product_id')) ? $errors->first('product_id') : ''}}
                    </div>
                </div>
            </div>

          <div class="form-group row">
              <label for="discount_code" class="col-sm-3">Discount Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="discount_code" id="discount_code" placeholder="Code" value="{{old('discount_code')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('discount_code')) ? $errors->first('discount_code') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
                <label for="discount_type" class="col-sm-3">Discount Type</label>
                <div class="form-input col-sm-9">
                    <select name="discount_type" id="discount_type" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Discount Type===</option>
                            <option value="Resign">Resign</option>
                            <option value="Resign">Resign</option>
                            <option value="Resign">Resign</option>
                            <option value="Resign">Resign</option>
                            <option value="Resign">Resign</option>
                    </select>
                    <div class="valid-feedback">
                      {{ ($errors->has('discount_type')) ? $errors->first('discount_type') : ''}}
                    </div>
                </div>
            </div>

            <div class="form-group row">
              <label for="discount_amount" class="col-sm-3">Amount</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="discount_amount" id="discount_amount" placeholder="Amount" value="{{old('discount_amount')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('discount_amount')) ? $errors->first('discount_amount') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="discount_expire_date" class="col-sm-3">Expiry Date</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="discount_expire_date" id="discount_expire_date" placeholder="Expired Date" value="{{old('discount_expire_date')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('discount_expire_date')) ? $errors->first('discount_expire_date') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="discount_description" class="col-sm-3">Description</label>
              <div class="form-input col-sm-9">
                  <textarea name="discount_description" cols="4" rows="5" value="{{old('discount_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="discount_description" required>{{ old('discount_description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('discount_description')) ? $errors->first('discount_description') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Add Dicount</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
	  $(document).ready(function() {
  	 		$(function(){
		      $("#discount_expire_date").datepicker({dateFormat: "yy-mm-dd"}).val();
		    });
		    $('#product_id').select2();
		    $('#discount_type').select2();
		});
</script>
@endsection