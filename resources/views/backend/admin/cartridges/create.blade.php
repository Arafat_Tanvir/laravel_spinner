@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Brand 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('cartridge_create') }}" class="btn btn-success"> All Brand List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Barnd Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('cartridge_store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
              <label for="cartridge_no" class="col-sm-3">Cartridage No</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control" name="cartridge_no" id="cartridge_no" placeholder="Cartridge No" value="{{old('cartridge_no')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('cartridge_no')) ? $errors->first('cartridge_no') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
                    <label for="color_id" class="col-sm-3">Color</label>
                    <div class="form-input col-sm-9">
                        <select name="color_id" id="color_id" class="form-control">
                            <option value="0" disabled="true" selected="true">===Select Color===</option>
                            @foreach($colors as $color)
                                <option value="{{$color->id }}">{{$color->color_name}}</option>
                            @endforeach
                        </select>
                        <div class="valid-feedback">
                            {{ ($errors->has('color_id')) ? $errors->first('color_id') : ''}}
                        </div>
                    </div>
                </div>

          <div class="form-group row">
              <label for="other_code" class="col-sm-3">Other Code</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control" name="other_code" id="other_code" placeholder="Other Code" value="{{old('other_code')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('other_code')) ? $errors->first('other_code') : ''}}
                  </div>
              </div>
          </div>


          <div class="form-group row">
              <label for="yield" class="col-sm-3">Yield</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control" name="yield" id="yield" placeholder="Yield" value="{{old('yield')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('yield')) ? $errors->first('yield') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="price" class="col-sm-3">Price</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="{{old('price')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('price')) ? $errors->first('price') : ''}}
                  </div>
              </div>
          </div>

          

      
          <button class="btn btn-primary" type="submit">Add Brand</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection