@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL cartridge List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('cartridge_create') }}" class="btn btn-success">  Add cartridge</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="cartridges" class="table table-bordered table-striped">
          <caption>List of cartridges</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Image</th>
  						<th>Description</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($cartridges as $cartridge)
  						<td>{{ $a++ }}</td>
  						<td>{{ $cartridge->cartridge_name }}</td>
                     
  						<td>
  						    @if($cartridge->cartridge_description)
		                  <p>{{ $cartridge->cartridge_description}}</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('cartridge_edit', $cartridge->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $cartridge->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$cartridge->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('cartridge_delete', $cartridge->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection