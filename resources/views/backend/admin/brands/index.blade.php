@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        ALL Brand List
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('brand_create') }}" class="btn btn-success">  Add Brand</a></li>
        
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-12 col-xs-12">
        	<div class="box">
        		<div class="box-header">
        			
        		</div>
        		<div class="box-body">
        			<div class="table-responsive mt-2">
        <table id="brands" class="table table-bordered table-striped">
          <caption>List of brands</caption>
          <thead>
  					<tr>
  						<th>SL</th>
  						<th>Name</th>
  						<th>Image</th>
  						<th>Description</th>
  						<th>Action</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<div style="display: none;">{{$a=1}}</div>
  						@foreach($brands as $brand)
  						<td>{{ $a++ }}</td>
  						<td>{{ $brand->brand_name }}</td>
                        
  						<td>
  						      @if($brand->brand_image)
			                  <p>
			                    <img class="" src="{{asset('assets/admin/images/brands/'.$brand->brand_image)}}" height="50px" width="200px">
			                  </p>
			                  @else
			                    <p>N/A</p>
			                  @endif
  						</td>
  						<td>
  						    @if($brand->brand_description)
		                  <p>{{ $brand->brand_description}}</p>
		                  @else
		                    <p>N/A</p>
		                  @endif
  						</td>
            
  						<td>
  							<a href="{{route('brand_edit', $brand->id)}}" class="badge badge-warning">Edit</a>
                <a href="#DeleteModal{{ $brand->id}}" data-toggle="modal" class="badge badge-danger btn-sm">Delete</a>
								<div class="modal fade" id="DeleteModal{{$brand->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="{{ route('brand_delete', $brand->id)}}" method="POST">
													{{csrf_field()}}
												<button type="submit" class="badge badge-success">Delete</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</a>
  						</td>
  					</tr>
  					@endforeach
  				</tbody>
        </table>
      </div>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection