@extends('Backend.admin.layouts.master')

@section('content')
<section class="content-header">
      <h1>
        Add Delivary 
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('delivery_index') }}" class="btn btn-success"> All Delivary List</a></li>
      </ol>
    </section>

 <section class="content" style="margin-top: 10px;">
      <div class="row">
        <div class="col-lg-8 col-xs-8 col-sm-offset-2">
        	<div class="box">
        		<div class="box-header">
        			<h4>Delivery Create</h4>
        		</div>
        		<div class="box-body">
        			<form method="POST" class="user" action="{{ route('delivery_store') }}" enctype="multipart/form-data">
          @csrf

          

          <div class="form-group row">
              <label for="delivery_name" class="col-sm-3">Discount Name</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="delivery_name" id="delivery_name" placeholder="Delivary Name" value="{{old('delivery_name')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('delivery_name')) ? $errors->first('delivery_name') : ''}}
                  </div>
              </div>
          </div>


            <div class="form-group row">
              <label for="delivery_amount" class="col-sm-3">Amount</label>
              <div class="form-input col-sm-9">
                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="delivery_amount" id="delivery_amount" placeholder="Delivery Amount" value="{{old('delivery_amount')}}" required>
                  <div class="valid-feedback">
                    {{ ($errors->has('delivery_amount')) ? $errors->first('delivery_amount') : ''}}
                  </div>
              </div>
          </div>

          <div class="form-group row">
              <label for="delivery_description" class="col-sm-3">Description</label>
              <div class="form-input col-sm-9">
                  <textarea name="delivery_description" cols="4" rows="5" value="{{old('delivery_description')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="delivery_description" required>{{ old('delivery_description')}}</textarea>
                  <div class="valid-feedback">
                    {{ ($errors->has('delivery_description')) ? $errors->first('delivery_description') : ''}}
                  </div>
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Add Delivary</button>
      </form>
        		</div>
        	</div>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
	  $(document).ready(function() {
  	 		$(function(){
		      $("#delivery_expire_date").datepicker({dateFormat: "yy-mm-dd"}).val();
		    });
		    $('#product_id').select2();
		    $('#delivery_type').select2();
		});
</script>
@endsection