@extends('frontend.layouts.master')
@section('content')

<div class="container">
  <div class="row">

  	<form method="POST" action="{{ route('customer_review_store') }}" >
          @csrf
          <input type="text" class="d-none" name="product_id" value="{{$product->id}}">
  
               @foreach($reviews as $review)
                        <div class="col-sm-2" id="well{{$review->id}}">
                           <input type="checkbox" class="checkbox-design" id="reviews{{$review->id}}" name="reviews[]" value="{{ $review->id}}" />
                        </div>                 
                @endforeach

 <button class="btn btn-primary" type="submit">Customer Review</button>
        </form>
               
         
  </div>
</div>

@endsection

@section('scripts')


	<script type="text/javascript">
		$(document).ready(function(){
		<?php foreach($reviews as $review) { ?>
		$('#reviews<?= $review->id ?>').on('change',function(){
			$('#well<?= $review->id ?>').css('background-color','green')
		});
		<?php } ?>

	});

</script>
@endsection