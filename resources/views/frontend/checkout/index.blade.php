@extends('frontend.layouts.master')
@section('content')

<div class="container">
        <div class="breadcum-area">
            <div class="breadcum-inner">
                <h3>Checkout</h3>
                <ol class="breadcrumb">                    
                    <li class="breadcrumb-item"><a href="{{ URL::to('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Checkout</a></li>
                    <li class="breadcrumb-item">
                    	<a href="javascript:void(0)">
                    		@if(session('step')==0)
                            	Shipping Address
                            @elseif(session('step')==1)
                            	Billing Address
                            @elseif(session('step')==2)
                            	Shipping Methods
                            @elseif(session('step')==3)
                            	Order Detail
                            @endif
                    	</a>
                    </li>
                </ol>
            </div>
        </div>
		<div class="checkout-area">
            <div class="row">
				<div class="col-12 col-lg-8">
                <input type="hidden" id="hyperpayresponse">
                <div class="alert alert-danger alert-dismissible" id="paymentError" role="alert" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==0) active @elseif(session('step')>0) active-check @endif" id="shipping-tab" data-toggle="pill" href="#pills-shipping" role="tab" aria-controls="pills-shpping" aria-expanded="true">Shipping Address</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==1) active @elseif(session('step')>1) active-check @endif" @if(session('step')>=1) id="billing-tab" data-toggle="pill" href="#pills-billing" role="tab" aria-controls="pills-billing" aria-expanded="true" @endif >Billing Address</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==2) active @elseif(session('step')>2) active-check @endif"  @if(session('step')>=2)  id="shipping-methods-tab" data-toggle="pill" href="#pills-shipping-methods" role="tab" aria-controls="pills-shipping-methods" aria-expanded="true"  @endif>Shipping Methods</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('step')==3) active @elseif(session('step')>3) active-check @endif"  @if(session('step')>=3)  id="order-tab" data-toggle="pill" href="#pills-order" role="tab" aria-controls="pills-order" aria-expanded="true"  @endif>Order Detail</a>
                        </li>
                    </ul>
                    
                    <div class="tab-content" id="pills-tabContent">
                        {{--Shipping tab--}}
                      <div class="tab-pane fade @if(session('step') == 0) show active @endif" id="pills-shipping" role="tabpanel" aria-labelledby="shipping-tab">
                        
                        <form action="{{ route('shipping_store')}}" method="POST">
                        	 @csrf

                           @php 
                           session(['step'=>'0'])
                           @endphp
                            <input type="hidden" name="checkout_by_cart" value="set_session_for_checkout">
                            <div class="form-row">

                              <div class="form-group col-md-6">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control field-validate" id="firstname" name="firstname" value="@if(session('shipping_address')){{session('shipping_address')->firstname}}@endif">
                                 <span class="help-block error-content" hidden>Please enter your first name</span>  
                              </div>

                              <div class="form-group col-md-6">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control field-validate" id="lastname" name="lastname" value="@if(session('shipping_address')){{session('shipping_address')->lastname}}@endif">
                                <span class="help-block error-content" hidden>Please enter your last name</span> 
                              </div>
                             
                              <div class="form-group col-md-6">
                                <label for="firstName">Address</label>
                                <input type="text" class="form-control field-validate" id="street" name="street" value="@if(session('shipping_address')){{session('shipping_address')->street}}@endif">
                                <span class="help-block error-content" hidden>Please enter your address</span> 
                              </div>
                              <div class="form-group col-md-6">
                                <label for="lastName">Country</label>
                                  <select class="form-control field-validate" id="entry_country_id" onChange="getZones();" name="countries_id">
                                      <option value="" selected>Select Country</option>
                                      
                                  </select>
                                <span class="help-block error-content" hidden>Please select your country</span> 
                              </div>

                              <div class="form-group col-md-6">
                                <label for="firstName">State</label>
                                <select class="form-control field-validate" id="entry_zone_id" name="zone_id">
                                      <option value="" selected>Select State</option>
                                       <option value="Other">Other</option>                      
                                </select>
                                <span class="help-block error-content" hidden>Please select your state</span> 
                              </div>

                              <div class="form-group col-md-6">
                                <label for="lastName">City</label>
                                <input type="text" class="form-control field-validate" id="city" name="city" value="@if(session('shipping_address')){{session('shipping_address')->city}}@endif">
                                <span class="help-block error-content" hidden>Please enter your city</span> 
                              </div>

                              <div class="form-group col-md-6">
                                <label for="lastName">Zip/Postal Code</label>
                                <input type="text" class="form-control" id="postcode" name="postcode" value="@if(session('shipping_address')){{session('shipping_address')->postcode}}@endif">
                                <span class="help-block error-content" hidden>Please enter your Zip/Postal Code</span> 
                              </div>	

                              <div class="form-group col-md-6">
                                <label for="lastName">Phone Number</label>
                                <input type="text" class="form-control" id="delivery_phone" name="delivery_phone" value="@if(session('shipping_address')){{session('shipping_address')->delivery_phone}}@endif">
                                <span class="help-block error-content" hidden>Please enter your valid phone number</span> 
                              </div>

                            </div>		
                            <div class="button"><button type="submit" class="btn btn-dark">Continue</button></div>
                    	</form>
                      </div>
                      
                      
                      {{--billing tab--}}
                      <div class="tab-pane fade @if(session('step') == 1) show active @endif" id="pills-billing" role="tabpanel" aria-labelledby="billing-tab">
                        <form name="signup" enctype="multipart/form-data" action="{{ URL::to('/checkout_billing_address')}}" method="post">
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="firstName">First Name</label>
                            <input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_firstname" name="billing_firstname" value="@if(session('billing_address')){{session('billing_address')->billing_firstname}}@endif">
                            <span class="help-block error-content" hidden>Please enter your first name</span>  
                          </div>
                          <div class="form-group col-md-6">
                            <label for="lastName">Last Name</label>
                            <input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_lastname" name="billing_lastname" value="@if(session('billing_address')){{session('billing_address')->billing_lastname}}@endif">
                            <span class="help-block error-content" hidden>Please enter your last name</span> 
                          </div>
                          {{--<div class="form-group col-md-6">--}}
                            {{--<label for="firstName">Company</label>--}}
                            {{--<input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_company" name="billing_company" value="@if(session('billing_address')){{session('billing_address')->billing_company}}@endif">--}}
                            {{--<span class="help-block error-content" hidden>Please enter your company name</span> --}}
                          {{--</div>--}}
                          <div class="form-group col-md-6">
                            <label for="firstName">Address</label>
                            <input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_street" name="billing_street" value="@if(session('billing_address')){{session('billing_address')->billing_street}}@endif">
                            <span class="help-block error-content" hidden>Please enter your address</span>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="lastName">Country</label>
                              <select class="form-control same_address_select" id="billing_countries_id"  onChange="getBillingZones();" name="billing_countries_id" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) disabled @endif @else disabled @endif  >
                                  <option value=""  >Select Country</option>
                                 
                              </select>
                              <span class="help-block error-content" hidden>Please select your country</span> 
                          </div>
                          <div class="form-group col-md-6">
                            <label for="firstName">State</label>
                            <select class="form-control same_address_select" id="billing_zone_id" name="billing_zone_id" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) disabled @endif @else disabled @endif  >
                                  <option value="" >Select State</option>
                                 
                                    <option value="Other" @if(session('billing_address')) @if(session('billing_address')->billing_zone_id == 'Other') selected @endif @endif>Other</option>
                              </select>
                              <span class="help-block error-content" hidden>Please select your state</span> 
                          </div>
                          <div class="form-group col-md-6">
                            <label for="lastName">City</label>
                            <input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_city" name="billing_city" value="@if(session('billing_address')){{session('billing_address')->billing_city}}@endif">
                            <span class="help-block error-content" hidden>Please enter your city</span>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="lastName">Zip/Postal Code</label>
                            <input type="text" class="form-control same_address"  @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_zip" name="billing_zip" value="@if(session('billing_address')){{session('billing_address')->billing_zip}}@endif">
                            <span class="help-block error-content" hidden>Please enter your Zip/Postal Code</span> 
                          </div>	
                          	
                          <div class="form-group col-md-6">
                            <label for="lastName">Phone Number</label>
                            <input type="text" class="form-control same_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) readonly @endif @else readonly @endif  id="billing_phone" name="billing_phone" value="@if(session('billing_address')){{session('billing_address')->billing_phone}}@endif">
                            <span class="help-block error-content" hidden>Please enter your valid phone number</span> 
                          </div>	  
                        </div>			
                        <div class="form-group">
                            <div class="form-check">
                              <label class="form-check-label">
                                  <input  class="form-check-input" id="same_billing_address" value="1" type="checkbox" name="same_billing_address" @if(session('billing_address')) @if(session('billing_address')->same_billing_address==1) checked @endif @else checked  @endif > Same shipping and billing address')
                              </label>
                            </div>
                        </div>
                        <div class="button"><button type="submit" class="btn btn-dark"> Continue</button></div>
                    </form>
              	</div>
                {{--shipping method tab--}}
                <div class="tab-pane fade @if(session('step') == 2) show active @endif" id="pills-shipping-methods" role="tabpanel" aria-labelledby="shipping-methods-tab">
                    <div class="shipping-methods">
                        <p class="title">Please select a prefered shipping method to use on this order</p>
                    <form name="shipping_mehtods" method="post" id="shipping_mehtods_form" enctype="multipart/form-data" action="{{ URL::to('/checkout_payment_method')}}">
                        
                       
                        <div class="alert alert-danger alert-dismissible error_shipping" role="alert" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Please select your shipping method')
                        </div>
                        <div class="button">
                            <button type="submit" class="btn btn-dark">Continue</button>
                        </div>
                      </form>
                    </div>
                </div>

                        {{--order tab--}}
                <div class="tab-pane fade @if(session('step') == 3) show active @endif" id="pills-order" role="tabpanel" aria-labelledby="order-tab"> 
                	 
                    <div class="order-review">
                        <?php 
                            $price = 0.00;
                        ?>
                        <form method='POST' id="order_form" action='{{ URL::to('/place_order')}}' >
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th align="left">items</th>
                                            <th align="right">Price</th>
                                            <th align="right">Qty</th>
                                            <th align="right">SubTotal</th>
                                        </tr>
                                    </thead>
                                  
                                  
                                     
                                    <tbody>
                                        
                                    </tbody>            
                                   
                                </table>
                            </div>                   
                           
                        </form>
                    </div>
                    <div class="notes-summary-area">
                    	<div class="heading">
                            <h2>orderNotesandSummary</h2>
                            <hr>
                        </div>
                    	<div class="row">
                        	<div class="col-xs-12 col-sm-6 order-notes">
                            	<p class="title">Please write notes of your order</p>
                                <div class="form-group">
                                    <p for="order_comments"></p>
                                    <textarea name="comments" id="order_comments" class="form-control" placeholder="Order Notes">@if(!empty(session('order_comments'))){{session('order_comments')}}@endif</textarea>
                                </div>
                            </div>
    
                            <div class="col-xs-12 col-sm-6 order-summary">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="payment-area">
                    	<div class="heading">
                            <h2>Payment Methods</h2>
                            <hr>
                        </div>
                        <div class="payment-methods">
                        <p class="title">Please select a prefered payment method to use on this order</p>
                        
                        <div class="alert alert-danger error_payment" style="display:none" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Please select your payment method')
                        </div>	
                                
                        <form name="shipping_mehtods" method="post" class="form-inline" id="payment_mehtods_form" enctype="multipart/form-data" action="{{ URL::to('/order_detail')}}">
                               <ul class="list">
                               
                               </ul>
                        </form>
                    </div>
                        
						<div class="button">
                            
                            <!--- paypal -->
                            <div id="paypal_button" class="payment_btns" style="display: none"></div>
                            
                            <button id="braintree_button" style="display: none" class="btn btn-dark payment_btns" data-toggle="modal" data-target="#braintreeModel" >Order Now</button>
                            
                            <button id="stripe_button" class="btn btn-dark payment_btns" style="display: none" data-toggle="modal" data-target="#stripeModel" >Order Now</button>
                            
                            <button id="cash_on_delivery_button" class="btn btn-secondary payment_btns" style="display: none; width:100% ;">Order Now</button>
                            <button id="instamojo_button" class="btn btn-dark payment_btns" style="display: none" data-toggle="modal" data-target="#instamojoModel">Order Now</button>
                            
                            <a href="{{ URL::to('/checkout/hyperpay')}}" id="hyperpay_button" class="btn btn-dark payment_btns" style="display: none">Order Now</a>
                                                        
                         </div>
                    </div>
                    
                                     
                
                    <!-- The braintree Modal -->
                    <div class="modal fade" id="braintreeModel">
                      <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="checkout" method="post" action="{{ URL::to('/place_order')}}">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">BrainTree Payment</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                      <div id="payment-form"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-dark">Pay</button>
                                </div>
                            </form>
                        </div>
                       </div>
                    </div>
                    
                    <!-- The instamojo Modal -->
                    <div class="modal fade" id="instamojoModel">
                      <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="instamojo_form" method="post" action="">
                            	
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Instamojo Payment</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                               <div class="modal-body">
                                      <div class="form-group row">
                                        <label for="firstName" class="col-sm-4 col-form-label">Full Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="fullname" class="form-control" placeholder="Full Name')" id="firstName">
                                            <span class="help-block error-content" hidden>Please enter your full name</span>
                                        </div>
                                     </div>
                                      <div class="form-group row">
                                        <label for="firstName" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="email_id" class="form-control " placeholder="Email')" id="email_id">
                                            <span class="help-block error-content" hidden>Please enter your email address</span>
                                        </div>
                                     </div>
                                      <div class="form-group row">
                                        <label for="firstName" class="col-sm-4 col-form-label">Phone Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="phone_number" class="form-control" placeholder="Phone Number')" id="insta_phone_number">
                                            <span class="help-block error-content" hidden>Please enter your valid phone number</span>
                                        </div>
                                     </div>
                                     <div class="alert alert-danger alert-dismissible" id="insta_mojo_error" role="alert" style="display: none">
                                        <span class="sr-only">Error'):</span>
                                        <span id="instamojo-error-text"></span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                   
                                </div>
                            </form>
                        </div>
                       </div>
                    </div>
                    
                    <!-- The stripe Modal -->
                    <div class="modal fade" id="stripeModel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            
                            <main>
                            <div class="container-lg">
                                <div class="cell example example2">
                                    <form>
                                      <div class="row">
                                        <div class="field">
                                          <div id="example2-card-number" class="input empty"></div>
                                          <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">Card number</label>
                                          <div class="baseline"></div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="field half-width">
                                          <div id="example2-card-expiry" class="input empty"></div>
                                          <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiration</label>
                                          <div class="baseline"></div>
                                        </div>
                                        <div class="field half-width">
                                          <div id="example2-card-cvc" class="input empty"></div>
                                          <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">CVC</label>
                                          <div class="baseline"></div>
                                        </div>
                                      </div>
                                    <button type="submit" class="btn btn-dark" data-tid="elements_examples.form.pay_button"></button>
                                    
                                      <div class="error" role="alert"><svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                                          <path class="base" fill="#000" d="M8.5,17 C3.80557963,17 0,13.1944204 0,8.5 C0,3.80557963 3.80557963,0 8.5,0 C13.1944204,0 17,3.80557963 17,8.5 C17,13.1944204 13.1944204,17 8.5,17 Z"></path>
                                          <path class="glyph" fill="#FFF" d="M8.5,7.29791847 L6.12604076,4.92395924 C5.79409512,4.59201359 5.25590488,4.59201359 4.92395924,4.92395924 C4.59201359,5.25590488 4.59201359,5.79409512 4.92395924,6.12604076 L7.29791847,8.5 L4.92395924,10.8739592 C4.59201359,11.2059049 4.59201359,11.7440951 4.92395924,12.0760408 C5.25590488,12.4079864 5.79409512,12.4079864 6.12604076,12.0760408 L8.5,9.70208153 L10.8739592,12.0760408 C11.2059049,12.4079864 11.7440951,12.4079864 12.0760408,12.0760408 C12.4079864,11.7440951 12.4079864,11.2059049 12.0760408,10.8739592 L9.70208153,8.5 L12.0760408,6.12604076 C12.4079864,5.79409512 12.4079864,5.25590488 12.0760408,4.92395924 C11.7440951,4.59201359 11.2059049,4.59201359 10.8739592,4.92395924 L8.5,7.29791847 L8.5,7.29791847 Z"></path>
                                        </svg>
                                        <span class="message"></span></div>
                                    </form>
                                                <div class="success">
                                                  <div class="icon">
                                                    <svg width="84px" height="84px" viewBox="0 0 84 84" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                      <circle class="border" cx="42" cy="42" r="40" stroke-linecap="round" stroke-width="4" stroke="#000" fill="none"></circle>
                                                      <path class="checkmark" stroke-linecap="round" stroke-linejoin="round" d="M23.375 42.5488281 36.8840688 56.0578969 64.891932 28.0500338" stroke-width="4" stroke="#000" fill="none"></path>
                                                    </svg>
                                                  </div>
                                                  <h3 class="title" data-tid="elements_examples.success.title">Payment successful</h3>
                                                  <p class="message"><span data-tid="elements_examples.success.message">Thanks You Your payment has been processed successfully</p>
                                                </div>
                            
                                            </div>
                                        </div>
                                    </main>
                                </div>
                        	</div>
                    	</div>
                	</div>

                    </div>
				</div> <!--CHECKOUT LEFT CLOSE-->
                
                <div class="col-12 col-lg-4 checkout-right">    
                    <div class="order-summary-outer">
                    	<div class="order-summary">
                            <div class="table-responsive">
                               
                            </div>
                        </div> 
                        <div class="coupons">
                            <form id="apply_coupon">
                                <div class="form-group">
                                    <label for="inputPassword2" class="">Coupon Code</label>
                                    <input type="text" name="coupon_code" class="form-control" id="coupon_code">
                                </div>
                                <button type="submit" class="btn-dark"><small>ApplyCoupon</small></button>
                                <div id="coupon_error" style="display: none"></div>
                                <div id="coupon_require_error" style="display: none">Please enter a valid coupon code</div>
                            </form>
                        </div>
                    </div>	
                </div>	<!--CHECKOUT RIGHT CLOSE-->
            </div>
		</div>
	</div>

<div class="container" style="border-top: 200px;">
  <div class="row">
  	<div class="col-sm-8">
  		<h4>Shipping Address</h4>
  		<form method="POST" action="{{ route('shipping_store') }}" >
          @csrf

          <div class="form-group row">
          	<label for="first_name" class="col-sm-3">First Name</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name">
          	</div>
          </div>

           <div class="form-group row">
          	<label for="last_name" class="col-sm-3">Last Name</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name">
          	</div>
          </div>

           <div class="form-group row">
          	<label for="phone_no" class="col-sm-3">Phone No</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="phone_no" class="form-control" id="phone_no" placeholder="Enter Phone No">
          	</div>
          </div>

           <div class="form-group row">
          	<label for="city" class="col-sm-3">Country</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="city" class="form-control" id="city" placeholder="Country">
          	</div>
          </div>

           <div class="form-group row">
          	<label for="city" class="col-sm-3">City</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="city" class="form-control" id="city" placeholder="City">
          	</div>
          </div>

           <div class="form-group row">
          	<label for="zipe" class="col-sm-3">Zipe</label>
          	<div class="form-input col-sm-9">
          		<input type="text" name="zipe" class="form-control" id="zipe" placeholder="Zipe">
          	</div>
          </div>

          <button class="btn btn-primary float-right" type="submit">Continue</button>
        </form>
  	</div>
  	<div class="col-sm-4">
  		<div class="card">
  			<div class="card-header">
  				<h4>Price Summary</h4>
  			</div>
  			<div class="card-body">
				
					@foreach(App\Models\Cart::total_Cards() as $card)
					<p>
						<strong>{{ $card->product->product_title}}</strong>
					</p>
					<p>
						Total : <strong>{{ $card->product->product_price}}</strong>*
						{{$card->product_quantity}}
						@php
						$total_this_price=0;
						$total_this_price+=$card->product->product_price * $card->product_quantity
						@endphp
						=<span>{{ $total_this_price}}</span>
					</p>
					@endforeach
					<a href="{{route('cart_index')}}"> Change Card Item</a>
				
					@php
					$total_price=0;
					@endphp
					@foreach(App\Models\Cart::total_Cards() as $card)
					@php
					$total_price+=$card->product->product_price * $card->product_quantity
					@endphp
					@endforeach

					<p> Total Price : <strong>{{ $total_price }}</strong></p>
				
  			</div>
  		</div>
  	</div>
  </div>
</div>

@endsection