@extends('frontend.layouts.master')
@section('content')

<div class="container">
  <div class="row">
  
      @foreach($products as $product)
       <div class="col-sm-3">
        <div class="card product-control">
          <div class="card-header">
             @php
                $i=1
                @endphp
                @foreach($product->images as $image)
                @if($i>0)
                <a href="">
                  <img class="" src="{{asset('assets/admin/images/products/'.$image->product_image)}}" height="200px" width="100%">
                </a>
                @endif
                @php
                $i--
                @endphp
                
                @endforeach
          </div>
          <div class="card-body">
            <a class="btn btn-primary" href="{{ route('customer_review_show',$product->id)}}"><i class="fa fa-star" aria-hidden="true"/></i>
</a>
            <div class="products-details">
              <a href=""><h5 class="card-title">{{$product->product_title}}</h5></a>
            </div>
            <div class="products-price">
              <p class="card-text">Price: {{$product->product_price}}</p>
            </div>
            
               @foreach($reviews as $review)
<button type="checkbox" name="reviews[]" style="color: {{$review->star}}" class="fa fa-star" aria-hidden="true" value="{{ $review->id}}" /></button>
                 
                @endforeach
          </div>
          <form class="form-inline" action="{{ route('cart_store', $product->id)}}" method="POST">
              @csrf
              <input type="hidden" name="product_id" value="{{$product->id}}">
              <button type="submit" class="btn btn-success">Add To Card</button>
            </form>
              </div>
              </div>
            @endforeach
      
  </div>
</div>

@endsection