<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\Cart;

class Cart extends Model
{

	public function user(){
    	return $this->belongsTo(User::class);
    }

    public function order(){
    	return $this->belongsTo(Order::class);
    }

    public function product(){
    	return $this->belongsTo(Product::class);
    }
    

    public static function total_Items()
    {
        if(Auth::check()){
            $cards=Cart::where('user_id',Auth::id())
            ->where('order_id',NULL)
            ->get();
            //dd($cards);
        }else{

             $cards=Cart::where('ip_address',request()->ip())
             ->where('order_id',NULL)
             ->get();
            //dd($cards);
        }
        $total_items=0;
        foreach ($cards as $card) {
            $total_items+=$card->product_quantity;
        }
        return $total_items;
    }

    //total card are show in this section 
    public static function total_Cards()
    {
        if(Auth::check()){
            $cards=Cart::where('user_id',Auth::id())
            ->where('order_id',NULL)
            ->get();
        }else{
             $cards=Cart::where('ip_address', request()->ip())
             ->where('order_id',NULL)
             ->get();
        }
        return $cards;
    }
}
