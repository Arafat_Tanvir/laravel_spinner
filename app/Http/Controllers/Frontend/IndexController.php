<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Models\Product;

class IndexController extends Controller
{
    public function index()
    {
    	$products =Product::orderBy('id','desc')->get();
    	$reviews =review::orderBy('id','desc')->get();
        return view('frontend.index',[
        	'products'=>$products,
        	'reviews'=>$reviews,
        ]);
    }
}
