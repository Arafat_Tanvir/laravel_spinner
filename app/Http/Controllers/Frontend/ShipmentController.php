<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShipmentController extends Controller
{
    public function store(Request $request)
	{
		if(session('step')=='0')
		{
			session(['step'=>'1']);
			
		}
		return redirect()->back();
	}
}
