<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;

class ReviewController extends Controller
{
    public function index()
	{
	    $reviews =review::orderBy('id','desc')->get();
	    return view('backend.admin.reviews.index',compact('reviews'));
	}

	public function create()
	{
	    return view('backend.admin.reviews.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'star'=>'required',
			'parcentage'=>'required'
		]);

		$review=new Review();
		$review->star=$request->star;
		$review->parcentage=$request->parcentage;
		$review->save();
		if(!is_null($review)){
			session()->flash('success','review create Successfully!!');
			return redirect()->route('review_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$review=Review::findOrFail($id);
		return view('backend.admin.reviews.show',compact('review'));
	}


	public function edit($id)
	{
		$review = Review::findOrFail($id);
		if(!is_null($review))
		{
			return view('backend.admin.reviews.edit',[
				'review'=>$review
			]);
		}else
		{
			return redirect()->route('review_index');
		}
	}


	public function update(Request $request, $id)
	{

		$review=Review::findOrFail($id);
		$review->star=$request->star;
		$review->parcentage=$request->parcentage;
		$review->save();
		if(!is_null($review)){
			session()->flash('success','review Update Successfully!!');
			return redirect()->route('review_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
