<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shipment;

class ShipmentController extends Controller
{
    public function index()
	{
	    $shipments =Shipment::orderBy('id','desc')->get();
	    return view('backend.admin.shipments.index',compact('shipments'));
	}

	public function create()
	{
	    return view('backend.admin.shipments.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'shipment_name'=>'required',
			'shipment_description'=>'required',
			'shipment_image'=>'required'
			
		]);

		$shipment=new Shipment();
		$shipment->shipment_name=$request->shipment_name;
		$shipment->shipment_description=$request->shipment_description;
		if($request->file('shipment_image'))
        {
        	$images=$request->file('shipment_image');
            $img=$request->shipment_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/shipments/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $shipment->shipment_image=$img;
        }
		$shipment->save();
		if(!is_null($shipment)){
			session()->flash('success','shipment create Successfully!!');
			return redirect()->route('shipment_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$shipment=Shipment::findOrFail($id);
		return view('backend.admin.shipments.show',compact('shipment'));
	}


	public function edit($id)
	{
		$shipment = Shipment::findOrFail($id);
		if(!is_null($shipment))
		{
			return view('backend.admin.shipments.edit',[
				'shipment'=>$shipment
			]);
		}else
		{
			return redirect()->route('shipment_index');
		}
	}


	public function update(Request $request, $id)
	{

		$shipment=Shipment::findOrFail($id);
		$shipment->shipment_name=$request->shipment_name;
		$shipment->shipment_description=$request->shipment_description;
		if($request->file('shipment_image'))
        {
        	$images=$request->file('shipment_image');
            $img=$request->shipment_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/shipments/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $shipment->shipment_image=$img;
        }
		$shipment->save();
		if(!is_null($shipment)){
			session()->flash('success','shipment Update Successfully!!');
			return redirect()->route('shipment_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
