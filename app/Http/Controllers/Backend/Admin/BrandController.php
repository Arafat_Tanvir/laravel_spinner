<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use Image;
use File;

class BrandController extends Controller
{
    public function index()
	{
	    $brands =Brand::orderBy('id','desc')->get();
	    return view('backend.admin.brands.index',compact('brands'));
	}

	public function create()
	{
	    return view('backend.admin.brands.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'brand_name'=>'required',
			'brand_description'=>'required',
			'brand_image'=>'required'
			
		]);

		$brand=new Brand();
		$brand->brand_name=$request->brand_name;
		$brand->brand_description=$request->brand_description;
		if($request->file('brand_image'))
        {
        	$images=$request->file('brand_image');
            $img=$request->brand_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/brands/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $brand->brand_image=$img;
        }
		$brand->save();
		if(!is_null($brand)){
			session()->flash('success','Brand create Successfully!!');
			return redirect()->route('brand_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$brand=Brand::findOrFail($id);
		return view('backend.admin.brands.show',compact('brand'));
	}


	public function edit($id)
	{
		$brand = Brand::findOrFail($id);
		if(!is_null($brand))
		{
			return view('backend.admin.brands.edit',[
				'brand'=>$brand
			]);
		}else
		{
			return redirect()->route('brand_index');
		}
	}


	public function update(Request $request, $id)
	{

		$brand=Brand::findOrFail($id);
		$brand->brand_name=$request->brand_name;
		$brand->brand_description=$request->brand_description;
		if($request->file('brand_image'))
        {
        	$images=$request->file('brand_image');
            $img=$request->brand_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/brands/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $brand->brand_image=$img;
        }
		$brand->save();
		if(!is_null($brand)){
			session()->flash('success','Brand Update Successfully!!');
			return redirect()->route('brand_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
