<?php

namespace App\Http\Controllers\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cartridge;
use App\Models\Color;

class CartridgeController extends Controller
{
    
    public function index()
	{
	    $cartridges =Cartridge::orderBy('id','desc')->get();
	    return view('backend.admin.cartridges.index',compact('cartridges'));
	}

	public function create()
	{
		$colors =Color::orderBy('id','desc')->get();
	    return view('backend.admin.cartridges.create',[
	    	'colors'=>$colors
	    ]);
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'cartridge_no' => 'required|unique:cartridges',
			'price'=>'required'
			
		]);

		$cartridge=new Cartridge();
		$cartridge->cartridge_no=$request->cartridge_no;
		$cartridge->other_code=$request->other_code;
		$cartridge->yield=$request->yield;
		$cartridge->price=$request->price;
		$cartridge->save();
		if(!is_null($cartridge)){
			session()->flash('success','cartridge create Successfully!!');
			return redirect()->route('cartridge_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}


	public function show($id)
	{
		$cartridge=cartridge::findOrFail($id);
		return view('backend.admin.cartridges.show',compact('cartridge'));
	}


	public function edit($id)
	{
		$cartridge = cartridge::findOrFail($id);
		if(!is_null($cartridge))
		{
			return view('backend.admin.cartridges.edit',[
				'cartridge'=>$cartridge
			]);
		}else
		{
			return redirect()->route('cartridge_index');
		}
	}


	public function update(Request $request, $id)
	{

		$cartridge=cartridge::findOrFail($id);
		$cartridge->cartridge_name=$request->cartridge_name;
		$cartridge->cartridge_description=$request->cartridge_description;
		if($request->file('cartridge_image'))
        {
        	$images=$request->file('cartridge_image');
            $img=$request->cartridge_name.'.'.$images->getClientOriginalExtension();
            $location=public_path('assets/admin/images/cartridges/'.$img);
            Image::make($images)->save($location)->resize(300,300);
            $cartridge->cartridge_image=$img;
        }
		$cartridge->save();
		if(!is_null($cartridge)){
			session()->flash('success','cartridge Update Successfully!!');
			return redirect()->route('cartridge_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
}
